# npm 

## npm 安装
推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。

```
npm i base-ui -S
```

## 使用

在 main.js 中全局引用。

```
import baseUi from 'base-ui'; // 引用组件
import 'base-ui/base-ui.css'; // 添加样式
Vue.use(base-ui);
```
