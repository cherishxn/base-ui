### 公共标题

pc 页面公共标题
::: demo

```html
<template>
  <h-common-title :configdata="configdata" direction="left" />
</template>
<script>
  export default {
    data() {
      return {
        configdata: {
          marginTop: 10,
          marginBottom: 10,
          title: '公共标题',
          msg: '介绍1111',
          isShowMore: true,
          rightMsg: '更多',
          openUrl: '这是一个跳转链接',
        },
      };
    },
  };
</script>
```

:::

### Attributes

| 参数       | 说明             | 类型   | 可选值 | 默认值 |
| ---------- | ---------------- | ------ | ------ | ------ |
| configdata | 配置标题的配置项 | object | 无     | 无     |

### Attributes Configdata

| 参数         | 说明               | 类型          | 可选值 | 默认值 |
| ------------ | ------------------ | ------------- | ------ | ------ |
| marginTop    | 上面的距离         | string/number | 无     | 无     |
| marginBottom | 下面的距离         | string/number | 无     | 无     |
| title        | 标题文字           | string        | 无     | 无     |
| msg          | 标题介绍           | string        | 无     | 无     |
| isShowMore   | 是否显示右侧更多   | boolean       | 无     | 无     |
| rightMsg     | 右侧更多的文字     | string        | 无     | 无     |
| openUrl      | 右侧更多的跳转链接 | string        | 无     | 无     |
