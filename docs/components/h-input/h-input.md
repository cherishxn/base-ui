### 输入框(未完善)

通过鼠标或键盘输入字符
::: demo

```html
<template>
  <h-input :message="message" />
</template>
<script>
  export default {
    data() {
      return {
        message: '输入框的值',
      };
    },
  };
</script>
```

:::

### Attributes

| 参数    | 说明   | 类型            | 可选值 | 默认值 |
| ------- | ------ | --------------- | ------ | ------ |
| message | 绑定值 | string / number | —      | —      |
