### 头部导航

组件介绍
::: demo

```html
<template>
  <div class="scroll-box">
    <h-header-nav :configdata="configdata" />
  </div>
</template>
<script>
  export default {
    data() {
      return {
        configdata: {
          welcomeSpeech: "欢迎语！",
          menuList: [
            {
              type: "dropdown",
              name: "有下拉框",
              child: [{ href: "", label: "链接label" }],
            },
            {
              type: "link",
              name: "链接",
              href: "",
            },
            {
              type: "qrCode",
              name: "二维码",
            },
          ],
          imageSrc: [
            {
              url: "",
              text: "二维码介绍",
            },
          ],
        },
      };
    },
  };
</script>
<style>
  .scroll-box {
    overflow-x: scroll;
  }
  .scroll-box::-webkit-scrollbar {
    width: 7px;
    height: 7px;
    background-color: #f5f5f5;
  }
  .scroll-box::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    border-radius: 10px;
    background-color: #f5f5f5;
  }
  .scroll-box::-webkit-scrollbar-thumb {
    border-radius: 10px;
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    background-color: #c8c8c8;
  }
</style>
```

:::

### Attributes

| 参数       | 说明   | 类型   | 可选值 | 默认值 |
| ---------- | ------ | ------ | ------ | ------ |
| configdata | 配置项 | object | 无     | 无     |

### Attributes Configdata

| 参数          | 说明               | 类型   | 可选值 | 默认值 |
| ------------- | ------------------ | ------ | ------ | ------ |
| welcomeSpeech | 欢迎语             | string | 无     | 无     |
| menuList      | 头部导航菜单       | array  | 无     | 无     |
| imageSrc      | 头部导航 LOGO 地址 | string | 无     | 无     |
