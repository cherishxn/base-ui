### 全部分类(有问题)

组件介绍
::: demo

```html
<template>
  <h-all-category :configdata="configdata" />
</template>
<script>
  export default {
    data() {
      return {
        configdata: {
          menuName: '菜单名称',
          menuSortArr: [
            {
              categoryName: '分类1',
              children: [
                {
                  categoryName: '分类1-1',
                  children: [
                    {
                      categoryName: '分类1-1-1',
                    },
                  ],
                },
                {
                  categoryName: '分类1-2',
                  children: [
                    {
                      categoryName: '分类1-2-1',
                    },
                    {
                      categoryName: '分类1-2-2',
                    },
                  ],
                },
              ],
            },
            {
              categoryName: '分类2',
              children: [
                {
                  categoryName: '分类2-1',
                  children: [
                    {
                      categoryName: '分类2-1-1',
                    },
                  ],
                },
              ],
            },
          ],
          channel: [
            {
              channelName: '不知道是啥',
            },
          ],
        },
      };
    },
  };
</script>
```

:::

### Attributes

| 参数       | 说明   | 类型   | 可选值 | 默认值 |
| ---------- | ------ | ------ | ------ | ------ |
| configdata | 配置项 | object | 无     | 无     |


### Attributes Configdata

| 参数 | 说明 | 类型 | 可选值 | 默认值 |
| ---- | ---- | ---- | ------ | ------ |
| ——   | ——   | ——   | ——     | ——     |
