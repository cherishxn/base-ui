### 限时秒杀

组件介绍
::: demo

```html
<template>
  <div class="scroll-box">
    <h-shop-time-seckill :configdata="configdata" />
  </div>
</template>
<script>
  export default {
    data() {
      return {
        configdata: {
          name: "限时秒杀",
          label: "shopTimeSeckill",
          type: "shopTimeSeckill",
          path: "shop",
          paddingTop: 20,
          paddingBottom: 20,
          backgroundImage: require("../../.vuepress/public/images/product/hotimg.png"),
          seckillData: [
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "2+16G全面屏 4K超高清 AI语音 教育投屏 网络智能液晶电视",
              shopPrice: 3599,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041120080000001540",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "2065d8dbedbc46169ae5a067933e8eed",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "海尔（Haier）商用空调中央空调嵌入式 大型天花机办公室 天井机 吊顶吸顶机",
              shopPrice: 6999,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041519310000001619",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "6eff5d11860042fa87aa5c1abe1518cd",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "康佳阿斐亚电视 55Z1 55英寸 OLED护眼 4K超高清全面屏 远场语音智慧屏 MEMC 3+32GB游戏液晶平板智能电视机",
              shopPrice: 4799,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041118040000001535",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "e5c843853a5e49e9badfe1367a362383",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                " 新一级能效 变频冷暖 自然风 智能互联 客厅圆柱空调立式柜机 鎏金版 ",
              shopPrice: 5099,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041525470000001634",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "c543dd989d7747eaa7521dd64850101f",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "海尔（Haier）超大型4\\/5P匹冷暖立柜式节能商用空调柜机包含4米铜管",
              shopPrice: 9000,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041520330000001622",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "894b26cd67cc47c19613a29fcf339a15",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "SONY 索尼 DSC-W830/W810/W800 便携相机/照相机/卡片机 高清摄像家用拍照 W830-黑色 官方标配 ",
              shopPrice: 2099,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041528450000001646",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "9351fdd2c39341cdae7068756fe35f6e",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "夏新移动空调制冷一体机 家用室内制冷无外机免排水安装 宿舍窗式单厨房客厅便捷式暖 新升级-大1.5匹冷暖（品牌压缩机+全年可用）",
              shopPrice: 1399,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041522170000001624",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "5086a741d3b4408688f4a788bce0695d",
            },
            {
              imageKey: require("../../.vuepress/public/images/product/hotimg.png"),
              productName:
                "格力（GREE）移动空调家用WIFI 单冷 智能除湿制冷器窗机二P商用立式便携式空调一体机",
              shopPrice: 3099,
              saleStock: 0,
              beginTime: "2024-11-30 00:00:00",
              endTime: "2024-11-30 00:00:00",
              skuNo: "SKU041523250000001627",
              promotionUuid: "111d0bcd4d0a4a31baa10bd4024d4d50",
              uuid: "2ec4493b65354a759c71fdf5375af683",
            },
          ],
          uuid: "c8895c63-775a-4149-9d7d-c5ad8e898a10",
          onlyName: "shopTimeSeckill1660014281856",
        },
      };
    },
  };
</script>
<style>
  .scroll-box {
    overflow-x: scroll;
  }
  .scroll-box::-webkit-scrollbar {
    width: 7px;
    height: 7px;
    background-color: #f5f5f5;
  }
  .scroll-box::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    border-radius: 10px;
    background-color: #f5f5f5;
  }
  .scroll-box::-webkit-scrollbar-thumb {
    border-radius: 10px;
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
    background-color: #c8c8c8;
  }
</style>
```

:::

### Attributes

| 参数       | 说明   | 类型   | 可选值 | 默认值 |
| ---------- | ------ | ------ | ------ | ------ |
| configdata | 配置项 | object | 无     | 无     |

### Attributes Configdata

| 参数          | 说明               | 类型   | 可选值 | 默认值 |
| ------------- | ------------------ | ------ | ------ | ------ |
| backgroundImage | 限时秒杀左侧栏背景  | string | 无     | 无     |
| paddingTop      | 顶部边距 | string  | 无     | 无     |
| paddingBottom      | 底部边距 | string | 无     | 无     |
| imageKey      | 商品图片地址 | string | 无     | 无     |
| productName      | 商品名称 | string | 无     | 无     |
| shopPrice      | 秒杀价格 | string | 无     | 无     |
| beginTime      | 秒杀开始时间 | string | 无     | 无     |
| endTime      | 秒杀结束时间 | string | 无     | 无     |
| saleStock      | 商品已售件数 | string/number | 无     | 无     | 
