---
home: true
# heroImage: /hero.png
heroText: BASE-UI 前端组件文档
tagline: 路漫漫其修远兮，吾将上下而求索
actionText: 快速上手 →
actionLink: /components/init
features:
- title: 一致性 Consistency
  details: 与现实生活一致：与现实生活的流程、逻辑保持一致，遵循用户习惯的语言和概念。
- title: 反馈 Feedback
  details: 通过界面样式和交互动效让用户可以清晰的感知自己的操作
- title: 效率 Efficiency
  details: 界面简单直白，让用户快速识别而非回忆，减少用户记忆负担。
---


::: slot footer
[全网前端开发组](https://www.qwang.com.cn/)
:::
