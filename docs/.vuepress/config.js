module.exports = {
  // port: 9090,
  theme: '', // 主题
  title: '全网前端组件文档管理',
  description: 'Vue 驱动的静态网站生成器',
  base: '/base-ui/',
  dest: './base-ui',
  head: [
    [
      'link',
     {
        rel: 'icon',
        href: '/favicons/favicon.png',
        type: 'image/png',
        sizes: '16x16',
      },
    ],
  ],
  themeConfig: {
    search: false, // 警用默认的搜索框
    nextLinks: true, // 上/先一篇链接
    prevLinks: true,
    // sidebar: [ // 全部显示
    //     '/guide/',
    //     '/config/base/',
    //     'config/high/'
    // ],
    nav: [
      {
        text: '首页',
        link: '/',
      },
      {
        text: '组件',
        link: '/components/init',
      },
      {
        text: 'vue',
        items: [
          { text: 'vue', link: '/vue/instance/' },
          { text: 'vue-router', link: '/vue/router/' },
          { text: 'geek', link: '/vue/geek/base/' },
        ]
      },
      { text: '小工具合集',
        items: [
        { text: 'linux命令', link: '/tools/linux-command/' },
        { text: 'docker命令', link: '/tools/docker-command/' },
        { text: 'kubectl命令', link: '/tools/k8s-command/' },
        { text: 'crictl命令', link: '/tools/cri-command/' },
        { text: 'git命令', link: '/tools/git-command/git' },
        ]
      }
    ],
    // sidebar: {//左侧列表
    //   '/components/': [
    //     'init','h-shop-enlarge/h-shop-enlarge','h-hot-product/h-hot-product'
    //   ],
    //   '/vue/': [
    //     'instance'
    //   ]
    // },
    sidebar: [
      {
        title: '安装',
        path: '/components/',
        collapsable: false,
        sidebarDepth: 1,
        children: [
          '/components/init',
        ],
      },
      // 分组显示
      {
        title: '商品组件',
        path: '/components/',
        collapsable: false,
        sidebarDepth: 1,
        children: [
          '/components/h-shop-enlarge/h-shop-enlarge.html',
          '/components/h-hot-product/h-hot-product.html',
        ],
      },
      {
        title: '基础组件',
        path: '/components/',
        collapsable: false,
        sidebarDepth: 1,
        children: [
          '/components/h-common-title/h-common-title.html',
          '/components/h-input/h-input.html',
          '/components/h-all-category/h-all-category.html',
          '/components/h-header-nav/h-header-nav.html', 
          '/components/h-shop-time-seckill/h-shop-time-seckill.html'
        ],
      },
      {
        title: '通知公告',
        collapsable: false,
        path: '/components/',
        sidebarDepth: 1,
        children: [
          '/components/h-notice/h-notice.html',
          '/components/h-icon/h-icon.html',
        ],
      },
      {
        title: 'vue文档',
        collapsable: false,
        path: '/vue/instance/',
        sidebarDepth: 1,
        children: [
          '/vue/instance.html',
        ],
      },
    ],
    sidebarDepth: 2, // 最大的深度为 2
    navbar: true, // 禁用导航栏
    displayAllHeaders: true, // 显示所有页面的标题链接 默认值：false
    // sidebar: 'auto', // 自动显示
  },
  plugins: ['demo-container'],
  markdown: {},
};
