import baseUi from '../components/index.js'
import iconfont from '../components/icon/iconfont.css'
import global from '../components/styles/global.scss'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

export default ({
	Vue // VuePress 正在使用的 Vue 构造函数
}) => {
	Vue.use(ElementUI)
	Vue.use(baseUi,iconfont,global)
}